package com.android.uiautomator.actions;

import com.android.uiautomator.UiAutomatorHelper;
import com.android.uiautomator.UiAutomatorModel;
import com.android.uiautomator.UiAutomatorViewer;
import com.android.uiautomator.actions.enums.Rotate;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

public class RotateImageLeftAction extends Action {

    private UiAutomatorViewer mViewer;

    public RotateImageLeftAction(UiAutomatorViewer viewer) {
        super("&Rotate left");
        mViewer = viewer;
    }

    @Override
    public ImageDescriptor getImageDescriptor() {
        return ImageHelper.loadImageDescriptorFromResource("images/rotate_left_16.png");
    }

    @Override
    public void run() {
        if (mViewer.getScreenShot() == null) return;
        Image img = mViewer.getScreenShot();
        ImageData imgData = ImageHelper.rotate(img.getImageData(), Rotate.LEFT);
        UiAutomatorModel model;
        try {
            model = new UiAutomatorModel(UiAutomatorHelper.getXmlDumpFile());
        } catch (Exception e) {
            // TODO add exception handler
            return;
        }
        img = new Image(Display.getDefault(), imgData);
        mViewer.setModel(model,img);

    }
}
