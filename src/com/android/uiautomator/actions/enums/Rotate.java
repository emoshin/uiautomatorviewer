package com.android.uiautomator.actions.enums;

public enum Rotate {
    LEFT,
    RIGHT
}
